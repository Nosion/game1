package game1;

/**
 * Created by 47724 on 16-10-2015.
 */
public interface Movable {
    public void Left();
    public void Right();
    public void Up();
    public void Down();
}
