package game1.models;

import game1.Movable;

/**
 * Created by 47724 on 16-10-2015.
 */
public abstract class BasePlayer extends BaseEntity implements Movable {

    @Override
    public void Left() {

        posX -= 10;
    }

    @Override
    public void Right() {
        posX += 10;
    }

    @Override
    public void Up() {
        posY -= 10;
    }

    @Override
    public void Down() {
        posY += 10;
    }


}
