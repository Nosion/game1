package game1.models;

import game1.Movable;

/**
 * Created by 47724 on 16-10-2015.
 */
public abstract class BaseEnemy extends BaseEntity implements Movable {

    @Override
    public void Left() {

    }

    @Override
    public void Right() {

    }

    @Override
    public void Up() {

    }

    @Override
    public void Down() {
    }

}
