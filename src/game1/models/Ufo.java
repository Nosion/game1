package game1.models;

import game1.AnimatedImage;

/**
 * Created by 47724 on 16-10-2015.
 */
public class Ufo extends BasePlayer {

    public Ufo() {
        sprite = new AnimatedImage("file:src\\game1\\gfx\\ufo\\ufo", ".png", 5);
    }

}