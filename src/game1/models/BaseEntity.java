package game1.models;

import game1.AnimatedImage;

/**
 * Created by 47724 on 16-10-2015.
 */
public class BaseEntity {
    int heightX;
    int heightY;
    int posX;
    int posY;
    AnimatedImage sprite;

    public int getHeightX() {
        return heightX;
    }

    public void setHeightX(int heightX) {
        this.heightX = heightX;
    }

    public int getHeightY() {
        return heightY;
    }

    public void setHeightY(int heightY) {
        this.heightY = heightY;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public AnimatedImage getSprite() {
        return sprite;
    }
}
