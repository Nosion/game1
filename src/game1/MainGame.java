package game1;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.image.Image;

public class MainGame extends Application {

  //  ArrayList<String> input = new ArrayList<String>();


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage theStage) {

        theStage.setTitle("Canvas Example");

        Group root = new Group();
        Scene theScene = new Scene(root);
        theStage.setScene(theScene);

        Canvas canvas = new Canvas(800, 480);
        root.getChildren().add(canvas);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setFill(Color.BLACK);
        gc.setLineWidth(2);
        Font font = Font.font("Calibri", FontWeight.BOLD, 14);
        gc.setFont(font);

        GameLoop gameLoop = new GameLoop(gc);

        //Sprites
        Image batman = new Image("file:src\\game1\\gfx\\batman.png", 100, 100, false, false);
        Image background = new Image("file:src\\game1\\gfx\\bg.png", 800, 480, false, false);



        //Keys press...
        theScene.setOnKeyPressed((e) -> {
            String code = e.getCode().toString();
            //Ufo ufo = new Ufo();


            if (code.equals("RIGHT")) {
                //gameLoop.ufoX += 10;
                //ufo.Right();
                gameLoop.moveRight = true;
            }
            if (code.equals("LEFT")) {
                //gameLoop.ufoX -= 10;
                gameLoop.moveLeft = true;
            }
            if (code.equals("UP")) {
                //gameLoop.ufoY -= 10;
                gameLoop.moveUp = true;
            }
            if (code.equals("DOWN")) {
                //gameLoop.ufoY += 10;
                gameLoop.moveDown = true;
            }

        });

        theScene.setOnKeyReleased((e) -> {
            String code = e.getCode().toString();


                if (code.equals("RIGHT")) {
                    //gameLoop.ufoX += 10;
                    //ufo.Right();
                    gameLoop.moveRight = false;
                }
                if (code.equals("LEFT")) {
                    //gameLoop.ufoX -= 10;
                    gameLoop.moveLeft = false;
                }
                if (code.equals("UP")) {
                    //gameLoop.ufoY -= 10;
                    gameLoop.moveUp = false;
                }
                if (code.equals("DOWN")) {
                    //gameLoop.ufoY += 10;
                    gameLoop.moveDown = false;
                }


        });

        //Mouse move
        theScene.setOnMouseClicked((e) -> {
            System.out.println("Bang!");

            // System.out.println();
            // if(e.getSceneX()

        });

        gameLoop.start();

        theStage.show();

    }



}
