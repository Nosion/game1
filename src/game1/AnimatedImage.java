/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game1;

import javafx.scene.image.Image;
import java.util.ArrayList;

/**
 *
 * @author 47724
 */
public class AnimatedImage {
    public double duration;
    public ArrayList<Image> frames;

    public AnimatedImage(String gfxUrl, String gfxFormat, int gfxAmount ) {
        frames = new ArrayList<>();
        duration = 0.100;
        for (int i = 0; i < gfxAmount; i++) {
            this.frames.add(new Image(gfxUrl + i + gfxFormat));
        }
    }

    public Image getFrame(double time)
    {
        int index = (int)((time % (frames.size() * duration)) / duration);
        return frames.get(index);
    }

}
