package game1;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import game1.models.Ufo;

/**
 * Created by 47724 on 16-10-2015.
 */
public class GameLoop extends AnimationTimer {
    double passedTime = 1;
    int fpsCount = 0;
    int fps = 0;
   /* double batmanX = 0;
    double batmanY = 0;*/
    double ufoX = 450;
    double ufoY = 25;
    final long startNanoTime = System.nanoTime();
    GraphicsContext gc;
    Ufo ufoPlayer = new Ufo();

    public boolean moveRight = false;
    public boolean moveLeft = false;
    public boolean moveUp = false;
    public boolean moveDown = false;

    public GameLoop(GraphicsContext gc) {
        this.gc = gc;


    }

    //Sprites
   /* Image batman = new Image("file:src\\game1\\gfx\\batman.png", 100, 100, false, false);*/
    Image background = new Image("file:src\\game1\\gfx\\bg.png", 800, 480, false, false);

    //Hitbox
  /*  Rectangle batmanHitbox = new Rectangle(((int) batmanX), (int) batmanY, (int) batman.getHeight(), (int) batman.getWidth());*/


    //Game loop
    @Override
    public void handle ( long currentNanoTime){
        double time = (currentNanoTime - startNanoTime) / 1000000000.0;

        //region FPS
        if (time > passedTime) {
            passedTime = time + 1.0;
            fps = fpsCount;
            fpsCount = 0;
        } else {
            fpsCount += 1;
        }

        if(moveRight) {ufoPlayer.Right();}
        if(moveLeft) {ufoPlayer.Left();}
        if(moveUp) {ufoPlayer.Up();}
        if(moveDown) {ufoPlayer.Down();}
        //endregion

 /*       batmanX = 232 + 128 * Math.cos(time);
        batmanY = 232 + 128 * Math.sin(time);
        batmanHitbox.x = (int) batmanX;
        batmanHitbox.y = (int) batmanY;*/

     /*   if (batmanHitbox.contains((int) ufoX, (int) ufoY)) {
            System.out.println("Baam! collision");
        }
*/
        gc.drawImage(background, 0, 0);
        gc.drawImage(ufoPlayer.getSprite().getFrame(time), ufoPlayer.getPosX(), ufoPlayer.getPosY());
        gc.fillText(String.valueOf(fps), 780, 20);
        //gc.drawImage(batman, batmanX, batmanY);
    }



}
